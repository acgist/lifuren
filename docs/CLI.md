# CLI API

## 命令

```
./lifuren[.exe] 命令 [参数...]
./lifuren[.exe] audio [bach|shikuang|beethoven] [pred|train] model_file [audio_file|dataset]
./lifuren[.exe] image [chopin|mozart|wudaozi]   [pred|train] model_file [image_file|dataset]
./lifuren[.exe] embedding dataset
./lifuren[.exe] [?|help]
```

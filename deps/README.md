# 依赖

|名称|版本|官网|
|:--|:--|:--|
|fltk|1.3.8|https://github.com/fltk/fltk|
|FFmpeg|6.1.1|https://github.com/ffmpeg/FFmpeg|
|OpenCV|4.10.0|https://github.com/opencv/opencv|
|spdlog|1.13.0|https://github.com/gabime/spdlog|
|yaml-cpp|0.8.0|https://github.com/jbeder/yaml-cpp|
|libtorch|2.6.0|https://github.com/pytorch/pytorch|
